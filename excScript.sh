# replace Name with your name 
#initializing the increment value of N
N=1
#loop on N
for n in $(seq 1 5)
do
£printing message on the terminal
echo "Replace with a suitable message to display on the terminal when running the script"
#creating a new folder name:malakn by replacing n by its value
mkdir malak${n}
#replacing the value of xxxx by N in the file input0rig and saving it to file input.txt
sed -e "s/xxxx/${N}/" inputOrig.txt > input.txt
cd malak${n}
cp ../input.txt ./
cd ../
N=$(( $N + $n ))
done
